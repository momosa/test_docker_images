test_cmd() {
    local -r DESCRIPTION=$1
    local -r TEST_CMD=$2
    local output
    local exit_status

    set +e
    output="$(eval $TEST_CMD 2>&1)"
    exit_status=$?
    set -e
    
    echo $DESCRIPTION
    if (( exit_status > 0 )); then
        echo TEST FAILED
        echo "$TEST_CMD"
        echo "$output"
        exit 1
    fi
}

test_command_available() {
    local -r COMMAND=$1
    test_cmd "Test command available: $COMMAND" \
        "which $COMMAND"
}

test_directory_exists() {
    local -r DIRECTORY=$1
    test_cmd "Test directory exists: $DIRECTORY" \
        "[ -d $DIRECTORY ]"
}

test_file_exists() {
    local -r FILE=$1
    test_cmd "Test file exists: $FILE" \
        "[ -f $FILE ]"
}

test_python_package_installed() {
    local -r PACKAGE=$1
    test_cmd "Test python package installed: $PACKAGE" \
        "python -c 'import $PACKAGE'"
}

test_python_package_version() {
    local -r PACKAGE=$1
    local -r VERSION=$2
    test_cmd "Test python package version: $PACKAGE $VERSION" \
        "pip list | grep $PACKAGE | grep $VERSION"
}
