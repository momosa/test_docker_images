#!/bin/bash -e

# Get the tests to execute
declare test_container="$(source fancy_pants_tests.sh)"

declare tests_passed=true

docker build --tag fancy_pants $PWD
# Run the container if it is persistent

echo
echo ========================================
echo "Testing fancy_pants"
echo ----------------------------------------

docker run fancy_pants bash -ce "$test_container" \
    || tests_passed=false

# If the container is running, use docker exec --tty instead

echo ========================================
echo

# Cleanup here: stop running containers, etc.

$tests_passed && echo Passed || { echo Failed; exit 1; }
