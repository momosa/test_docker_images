cat <<-'EOF'

source /test_functions.sh

test_command_available vim
test_python_package_installed ipdb
test_python_package_version ipdb 0.12
test_directory_exists /my_stuff
test_file_exists /my_stuff/my_file.txt

EOF
