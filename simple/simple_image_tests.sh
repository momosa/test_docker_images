cat <<-'EOF'

echo 'Test that vim is available'
if ! which vim; then
    echo 'FAILURE: vim command not found'
    exit 1
fi

echo 'Test that python package ipdb is installed'
if ! python -c 'import ipdb'; then
    echo 'FAILURE: ipdb not installed'
    exit 1
fi

echo 'Test that ipdb is version 0.12'
if ! pip list | grep ipdb | grep 0.12; then
    echo 'FAILURE: ipdb is not the correct version'
    exit 1
fi

echo 'Test that directory /my_stuff was created'
if ! [ -d /my_stuff ]; then
    echo 'FAILURE: directory /my_stuff does not exist'
    exit 1
fi

echo 'Test that file /my_stuff/my_file.txt was copied'
if ! [ -f /my_stuff/my_file.txt ]; then
    echo 'FAILURE: file /my_stuff/my_file.txt does not exist'
    exit 1
fi

EOF
