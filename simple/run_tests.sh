#!/bin/bash -e

# Get the tests to execute
declare test_container="$(source simple_image_tests.sh)"

declare tests_passed=true

docker build --tag simple_image $PWD
# Run the container if it is persistent

echo
echo ========================================
echo "Testing simple_image"
echo ----------------------------------------

docker run simple_image bash -ce "$test_container" \
    || tests_passed=false

# If the container is running, use docker exec --tty instead

echo ========================================
echo

# Cleanup here: stop running containers, etc.

$tests_passed && echo Passed || echo Failed
