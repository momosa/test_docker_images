# Testing docker images

## Contents

1. [About this repo](#about)
1. [Why test images?](#why)
1. [Testing a simple image](#simple)
1. [Clarifying tests with fancy functions](#fancy)
1. [The real world](#improvements)

- - -

<a name="about"></a>

## 1\. About this repo

This repo provides a couple of examples for testing docker images.
There are two examples,
a [simple](#simple) one and
a [fancier](#fancy) one that streamlines test files with functions.
The examples can be extended to cover a variety of use cases.

Knowledge prerequisites: docker basics, shell scripting basics.

To run an example,
first make sure you have [docker](https://docs.docker.com/install/) installed.
Then go into the corresponding directory and execute `run_tests.sh`.

```
# From project root:
cd simple
./run_tests.sh

# From project root:
cd fancy
./run_tests.sh
```

Each will build a simple docker image and run a set of tests.
The process is explained in more detail below.

- - -

<a name="why"></a>

## 2\. Why test images?

Probably the same reasons why you're here reading this.
Docker files can get long and messy.
The creation process often looks something like this:

1. Create a big `Dockerfile`
1. Build the image. If it fails, use the internet until it works.
1. Run a container. This is testing, folks.
1. If it doesn't work as expected, hop into the container and try to debug the problem.
1. Repeat until things do what you want.
1. Never look at it again... until it breaks, something needs upgrading, etc.

This looks like a job for an iterative, repeatable process.

Before moving on, let's look at the container-debugging step.
We hop into a container using either `docker run` or,
if the container is the sort to run in the background,
we run it and then dive in using `docker exec -ti`.
Then we perform some checks:

- Is some command available? `which my_command`
- Is some python package installed? `python -c 'import the_package'`
- Etc.

Once we find something wrong,
we've narrowed the problem and can proceed with debugging.

We can automate this first debugging phase since
it just consists of performing various checks.
Doing so enables us to work iteratively as we create a dockerfile,
performing checks along the way.
It also amounts to creating an apparatus for repeatable testing.

- - -

<a name="simple"></a>

## 3\. Testing a simple image

The simple image example is in the `simple/` directory.

We want to:

1. Build an image using a dockerfile. (If the container runs in the background, spin one up.)
1. Execute test commands in the container to validate it.
1. Know whether or not the validation is successful, and get some useful feedback if it wasn't.

We'll do this using two files:

1. `run_tests.sh`: A test-runner script to build the image, run a container, and execute tests.
1. `simple_image_tests.sh`: A script to designate tests for our image.

To run the tests:

```
# From the project repo simple/ directory:
./run_tests.sh
```

### 3.1\. Test-runner `run_tests.sh`

The test-runner `run_tests.sh` does the following:

1. Build the image
1. Test the container
1. Report validation status

Notice the `test_container` variable.
This should be understood in its verb form,
as in "Test the container! Do it now!",
not "These are tests for the container".
`test_container` comes from `simple_image_tests.sh`,
which contains the tests specific to our image.
`test_container` is just a string of shell commands.

To see how it works, you can do this before running the tests:

```
test_container='echo hygiene is important'
```

When you execute the test runner,
your command will be run from within the container.

### 3.2\. Image tests `simple_image_tests.sh`

These are the tests that will run inside a container created from the image.
We saw that the test-runner needs to set `test_container` as a string.
We'll output that string in `simple_image_tests.sh`.
This string can contain several tests,
so we'll use a heredoc to cleanly define them all.

For example, we can test that a command is available.
The check that we would normally perform in a docker container
is:

```
which vim
```

We can create a test around this as follows:

```
echo 'Test that vim is available'
if ! which vim; then
    echo 'FAILURE: vim command not found'
    exit 1
fi
```

See `simple_image_tests.sh` for a more complete and varied list.

- - -

<a name="fancy"></a>

## 4\. Clarifying tests with fancy functions

The fancy pants example is in the `fancy/` directory.

### 4.1\. Consolidating functions 

In the [simple](#simple) example above,
we can see how the heredoc can get pretty huge.
Also, there's a lot of repetition.
And in most real-world circumstances,
we'll want to perform the same test multiple times,
like checking that various commands are available.

We can consolitate by defining functions.
This will make the image-specific tests easier to read and write.
We can also use this opportunity to improve our tests
without messing with _every_ one.

See `test_functions.sh`.
We define a function `test_cmd`
that takes a description and a command.
It executes the command and provides improved feedback
based on the execution's exit status.
Then we define several other functions that use
`test_cmd` to perform specific checks,
which can be anything we want.
For example, testing that a command is available
or that a python package is installed.

Our image-specific tests are in `fancy_pants_tests.sh`.
We can see how this is more condensed and clear than the simple case,
plus we can improve many tests at once by adjusting the relevant functions.

```
test_command_available vim
test_python_package_installed ipdb
test_python_package_version ipdb 0.12
test_directory_exists /my_stuff
test_file_exists /my_stuff/my_file.txt
```

### 4.2\. Pluggin in test functions

To make these test functions work,
we need to ensure two things:

1. The test functions are available in the container.
1. `fancy_pants_tests.sh` knows about the test functions.

To make the test functions available in the container,
we'll use the dockerfile to copy the script:

```
COPY test_functions.sh /
```

Pro-tip: Place the copy command at the end of the dockerfile.
That way, you won't need to go through the other build steps
every time you change the test functions.
Plus, the copy is fast enough that there's no penalty
for repeating that step if you change something above it.

To make the test functions available in the image-specific tests,
we'll just source them in the heredoc.

```
source /test_functions.sh
```

Notice that the location, `/`,
is where we copied the function script in the dockerfile

- - -

<a name="improvements"></a>

## 5\. The real world

Use cases differ,
so you'll want to tailor the fancy example to meet your needs.
Here are a few recommendations, for starters:

1. The usual stuff with scripts: extra hardening, execution location agnosticism, etc.
1. Have the test-runner take the image as an input parameter. Then use the parameter to designate the image-specific test file, the image tag, and the container name.
1. Have the test-runner handle both persistent containers that run in the background and non-persistent containers.
1. Implement a proper cleanup phase so you don't have a bunch of old images and containers all over the place.
1. Create a meta-runner to test multiple images in one go.

Enjoy!!!
